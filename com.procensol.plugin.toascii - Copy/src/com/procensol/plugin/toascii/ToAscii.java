package com.procensol.plugin.toascii;

import com.appiancorp.services.ServiceContext;
import com.appiancorp.suiteapi.expression.annotations.AppianScriptingFunctionsCategory;
import com.appiancorp.suiteapi.expression.annotations.Function;
import com.appiancorp.suiteapi.expression.annotations.Parameter;
/*
 * @Auther: Kin Lee
 */
@AppianScriptingFunctionsCategory
public class ToAscii {

	@Function
	public int toasciidec(ServiceContext sc, @Parameter String input) throws InputException{
		
		if(input.length()==0)
			return 0;
		if(input.length()>2)
			throw new InputException("Invalid input.");
		
		if(input.length()==2)
			if(input.charAt(0)!='\\')
				throw new InputException("Invalid input.");
			else {
				String t="";
				switch(input.charAt(1)) {
				case 'n':	t="\n"; break;
				case 'r':	t="\r"; break;
				case 'b':	t="\b"; break;
				case 't':	t="\t"; break;
				case 'f':	t="\f"; break;
				case '"':	t="\""; break;
				case '\\':	t="\\"; break;
				default: break;
				}
				return (int)(t.substring(0, 1)).charAt(0);
			}
		
		return (int)input.charAt(0);
		}
	@Function
	public String toasciihexa(ServiceContext sc, @Parameter String input) throws InputException{
		
		return Integer.toHexString(toasciidec(sc,input));
		}
	
}
