package com.procensol.plugin.toascii;

import javax.naming.Context;

import com.appiancorp.services.ServiceContext;
import com.appiancorp.suiteapi.content.ContentService;
import com.appiancorp.suiteapi.process.framework.AppianSmartService;
import com.appiancorp.suiteapi.process.framework.SmartServiceContext;
import com.appiancorp.suiteapi.process.palette.PaletteInfo;
import com.appiancorp.suiteapi.security.external.SecureCredentialsStore;


@PaletteInfo(paletteCategory = "Procensol Smart Services", palette = "Utilities")
abstract class BaseSmartService extends AppianSmartService {

	/** The ctx. */
	final Context ctx;

	final ServiceContext sc;
	/** The content srvs. */
	final ContentService contentSrvs;

	final SmartServiceContext smartServiceCtx;

	final SecureCredentialsStore scs;

	public BaseSmartService(Context ctx, ServiceContext sc, SmartServiceContext smartServiceCtx, ContentService contentSrvc,
			SecureCredentialsStore scs) {
		super();
		this.ctx = ctx;
		this.sc = sc;
		this.contentSrvs = contentSrvc;
		this.smartServiceCtx = smartServiceCtx;
		this.scs = scs;
	}

}
