package com.procensol.plugin.toascii;

import javax.naming.Context;

import org.apache.log4j.Logger;

import com.appiancorp.services.ServiceContext;
import com.appiancorp.suiteapi.content.ContentService;
import com.appiancorp.suiteapi.process.exceptions.SmartServiceException;
import com.appiancorp.suiteapi.process.framework.SmartServiceContext;
import com.appiancorp.suiteapi.security.external.SecureCredentialsStore;

public class TestSS extends BaseSmartService {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(TestSS.class);


	/** The result. */
	String result;

	/**
	 * Instantiates a new gets the database metadata.
	 *
	 * @param smartServiceCtx the smart service ctx
	 * @param ctx             the ctx
	 * @param contentSrvc     the content srvc
	 * @param appSrvc         the app srvc
	 */
	public TestSS(SmartServiceContext smartServiceCtx,ServiceContext sc,  ContentService contentSrvs, Context ctx,
			SecureCredentialsStore scs) {
		super(ctx, sc, smartServiceCtx, contentSrvs, scs);
	}

	@Override
	public void run() throws SmartServiceException {
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>> Hello World!!");
		
	}


}
