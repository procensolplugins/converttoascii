package com.procensol.plugin.toascii;
/*
 * @Auther: Kin Lee
 */
public class InputException extends java.lang.Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InputException(String errorMessage) {
		super(errorMessage);
	}
}
